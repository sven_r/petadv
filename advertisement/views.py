from django.shortcuts import render
from django.views.generic import DetailView, ListView

from advertisement.models import Advertisement


class AdvertisementList(ListView):
    model = Advertisement
    template_name = 'advertisement/list.html'


class AdvertisementDetail(DetailView):
    model = Advertisement
    template_name = 'advertisement/detail.html'

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        self.object.view_count += 1
        self.object.save()
        return response







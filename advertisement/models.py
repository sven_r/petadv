from django.db import models


class Rubric(models.Model):
    title = models.CharField('заголовок', max_length=256)


class Advertisement(models.Model):
    title = models.CharField('заголовок', max_length=256)
    description = models.TextField('описание')
    price = models.IntegerField(default=0)
    rubric = models.ForeignKey(Rubric, on_delete=models.SET_NULL, null=True, blank=True)
    type = models.CharField(max_length=128, default='sell', choices=(
        ('sell', 'Продажа'),
        ('buy', 'Покупка')
    ))
    contact_info = models.TextField()
    view_count = models.IntegerField(default=0)

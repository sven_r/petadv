from django.contrib import admin
from django.urls import path

from advertisement.views import AdvertisementDetail, AdvertisementList

urlpatterns = [
    path('', AdvertisementList.as_view(), name='advertisement_list'),
    path('detail/<int:pk>', AdvertisementDetail.as_view(), name='advertisement_detail'),
]
